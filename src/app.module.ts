import { Module } from '@nestjs/common';
import { CallModule } from './call/call.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [CallModule, UsersModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
