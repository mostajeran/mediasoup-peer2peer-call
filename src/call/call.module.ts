import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { CallService } from './call.service';
import { CallGateway } from './gateways/call.gateway';

@Module({
  providers: [CallService, CallGateway],
  imports: [UsersModule],
})
export class CallModule {}
