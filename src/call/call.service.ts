import { Injectable } from '@nestjs/common';
import { Session } from './models/session.model';
import { Router, RtpCapabilities, Worker } from 'mediasoup/lib/types';
import { config } from '../config/config';
import { transportProperties } from './interfaces-types/transport-properties.interface';
import { User } from '../users/entities/user.entity';
import { Call } from './entities/call.entity';

@Injectable()
export class CallService {
  private Worker: Worker;
  private mediasoupRouter: Router;
  private sessionStorage = new Map<number, Session>(); // (key: value) userId, Session
  private callStorage = new Map<number, Call>(); // (key: value) callId, Call

  async createWebRtcTransport(): Promise<transportProperties> {
    const maxIncomingBitrate =
      config.mediasoup.additionalWebRtcTransport.maxIncomingBitrate;
    const {
      initialAvailableOutgoingBitrate,
    } = config.mediasoup.webRtcTransport;

    const transport = await this.mediasoupRouter.createWebRtcTransport({
      listenIps: config.mediasoup.webRtcTransport.listenIps,
      enableUdp: true,
      enableTcp: true,
      preferUdp: true,
      initialAvailableOutgoingBitrate,
    });
    if (maxIncomingBitrate) {
      try {
        await transport.setMaxIncomingBitrate(maxIncomingBitrate);
      } catch (error) {}
    }
    return {
      transport,
      params: {
        id: transport.id,
        iceParameters: transport.iceParameters,
        iceCandidates: transport.iceCandidates,
        dtlsParameters: transport.dtlsParameters,
      },
    };
  }

  getRouterRtpCapabilities(): RtpCapabilities {
    return this.mediasoupRouter.rtpCapabilities;
  }

  createNewSession(user: User, socketId: string): Session {
    if (!user.id) throw new Error('userId must not be empty!');
    const newSession = new Session({ user, socketId });
    this.sessionStorage.set(user.id, newSession);
    return newSession;
  }

  closeSession(userId: number): void {
    this.sessionStorage.delete(userId);
  }

  checkHasSession(userId: number): boolean {
    return this.sessionStorage.has(userId);
  }

  getSessionDetails(userId: number): Session {
    const session = this.sessionStorage.get(userId);
    if (!session) {
      throw new Error('session not found!');
    }
    return session;
  }

  saveCall(data: Partial<Call>): Call {
    if (!data.callerUserID || !data.calledUserID || !data.type)
      throw new Error("callerId, CalledId, type can't be null");

    const call = new Call(data.callerUserID, data.calledUserID, data.type);
    this.callStorage.set(call.id, call);
    return call;
  }

  getCall(callId: number): Call {
    const call = this.callStorage.get(callId);
    if (!call) throw new Error('call not found');
    return call;
  }
}
