import { CallType } from '../enums/call-type.enum';

export class Call {
  id: number;
  callerUserID: number;
  calledUserID: number;
  type: CallType;
  accepted: boolean;

  // auto increament id generator
  private static counter = 0;

  constructor(
    callerUserID: number,
    calledUserID: number,
    type: CallType,
    accepted?: boolean,
  ) {
    this.id = Call.counter++;
    this.callerUserID = callerUserID;
    this.calledUserID = calledUserID;
    this.type = type;
    if (typeof accepted != 'undefined') this.accepted = accepted;
  }
}
