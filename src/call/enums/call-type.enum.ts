export enum CallType {
  VOICE_CALL = 'voice-call',
  VIDEO_CALL = 'video-call',
}
