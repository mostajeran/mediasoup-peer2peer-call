import {
  OnGatewayDisconnect,
  OnGatewayConnection,
  SubscribeMessage,
} from '@nestjs/websockets';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Session } from '../models/session.model';
import { Server, Socket } from 'socket.io';
import { UsersService } from '../../users/users.service';
import { CallService } from '../call.service';
import { InitSessionPayload } from '../interfaces-types/init-session-payload.interface';
import { DialPayload } from '../interfaces-types/dial-payload.interface';
import { AcceptCallPayload } from '../interfaces-types/accept-call-payload.interface';
import { EndCallPayload } from '../interfaces-types/end-call-payload.interface';
import { Call } from '../entities/call.entity';

type ExtendedSocket = Socket & { session: Session };

@WebSocketGateway()
export class CallGateway implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    private readonly callService: CallService,
    private readonly usersService: UsersService,
  ) {}

  @WebSocketServer()
  server: Server;

  handleConnection(client: ExtendedSocket): void {
    console.log('client has connected id: ', client.id);
  }
  handleDisconnect(client: ExtendedSocket): void {
    console.log('client has disconnected id: ', client.id);
  }

  @SubscribeMessage('INIT_SESSION')
  initSession(client: ExtendedSocket, data: InitSessionPayload): void {
    if (!data.name) {
      client.emit('ERROR', "name can't be null");
      return;
    }

    client.session = this.callService.createNewSession(
      this.usersService.createUser({ name: data.name, avatar: data.avatar }),
      client.id,
    );

    client.emit(
      'RTP_CAPABILITIES',
      this.callService.getRouterRtpCapabilities(),
    );
  }

  @SubscribeMessage('DIAL')
  dialUser(client: ExtendedSocket, data: DialPayload): void {
    if (!client.session) {
      client.emit('ERROR', "you don't registred yet.");
      return;
    }
    if (!this.callService.checkHasSession(data.calledUserID)) {
      client.emit('ERROR', 'user is offline.');
      return;
    }
    const call = this.callService.saveCall({
      callerUserID: client.session.user.id,
      calledUserID: data.calledUserID,
      type: data.type,
    });
    this.sendCallRequest(call);
  }

  @SubscribeMessage('ACCEPT_CALL')
  acceptCall(client: ExtendedSocket, data: AcceptCallPayload): void {
    if (!client.session) {
      client.emit('ERROR', "you don't registred yet.");
      return;
    }
    const call = this.callService.getCall(data.callId);
    if (!data.accept) {
      this.notifyRejectedCall(call);
    } else {
      const calledTransport = null;
      const callerTransport = null;

      this.sendTransport(call.callerUserID, callerTransport);
      this.sendTransport(call.calledUserID, calledTransport);
    }
  }

  @SubscribeMessage('END_CALL')
  endCall(client: ExtendedSocket, data: EndCallPayload): void {
    if (!client.session) {
      client.emit('ERROR', "you don't registred yet.");
      return;
    }
    // TODO implement later
  }

  sendCallRequest(call: Call): void {
    try {
      const calledUserSession = this.callService.getSessionDetails(
        call.calledUserID,
      );
      this.server.to(calledUserSession.socketId).emit('INCOMING_CALL', call);
    } catch (e) {
      console.log(e);
    }
  }

  notifyRejectedCall(call: Call) {
    try {
      const callerUserSession = this.callService.getSessionDetails(
        call.callerUserID,
      );
      this.server.to(callerUserSession.socketId).emit('CALL_REJECTED', call);
    } catch (e) {
      console.log(e);
    }
  }

  sendTransport(userId: number, transport: unknown) {
    try {
      const callerUserSession = this.callService.getSessionDetails(userId);
      this.server
        .to(callerUserSession.socketId)
        .emit('TRANSPORT_DETAILS', transport);
    } catch (e) {
      console.log(e);
    }
  }
}
