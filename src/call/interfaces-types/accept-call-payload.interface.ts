export interface AcceptCallPayload {
  accept: boolean;
  callId: number;
}
