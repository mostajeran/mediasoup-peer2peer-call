import { CallType } from '../enums/call-type.enum';

export interface DialPayload {
  calledUserID: number;
  type: CallType;
}
