export interface EndCallPayload {
  end: boolean;
  callId: number;
}
