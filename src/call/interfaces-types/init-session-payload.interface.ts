export interface InitSessionPayload {
  name: string;
  avatar: string;
}
