import {
  Transport,
  IceParameters,
  IceCandidate,
  DtlsParameters,
} from 'mediasoup/lib/types';
export interface transportProperties {
  transport: Transport;
  params: {
    id: string;
    iceParameters: IceParameters;
    iceCandidates: IceCandidate[];
    dtlsParameters: DtlsParameters;
  };
}
