import { User } from '../../users/entities/user.entity';
import { Consumer, Producer, Transport } from 'mediasoup/lib/types';

export class Session {
  public user: User;
  public socketId: string;
  public incomingTransport: Transport;
  public outgoingTransport: Transport;
  public consumers: Consumer[];
  public producers: Producer[];

  constructor(params?: { user?: User; socketId?: string }) {
    if (params?.user) this.user = params?.user;
    if (params?.socketId) this.socketId = params?.socketId;
  }
}
