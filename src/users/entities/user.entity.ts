export class User {
  public id: number;
  public name: string;
  public avatar: string;

  // auto increament id generator
  private static counter = 0;

  constructor(name?: string, avatar?: string) {
    this.id = User.counter++;
    if (name) this.name = name;
    if (avatar) this.avatar = avatar;
  }
}
