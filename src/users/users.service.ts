import { Injectable } from '@nestjs/common';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  // this local storage uses Map for rapid storage system
  private userStorage = new Map<number, User>();

  getUserById(userId: number): User {
    const user = this.userStorage.get(userId);
    if (!user) throw new Error('User not found!');
    return user;
  }

  createUser(user: Partial<User>): User {
    const newUser = new User(user.name, user.avatar);
    this.userStorage.set(newUser.id, newUser);
    return newUser;
  }
}
